#include <cassert>
#include <iostream>

#include <fmt/core.h>

#include "statefile.hpp"

StateFile::StateFile(std::filesystem::path const &path_): path(path_) {
    this->fd.open(this->path, std::ios_base::in | std::ios_base::app);
    if (!fd.is_open()) {
        throw std::runtime_error(fmt::format("failed to open state file at '{}'", this->path.string()));
    }
}

StateFile::~StateFile() {
    this->fd.close();
}

std::vector<StateFile::class_rule_workspace> StateFile::deser() {
    using return_type = std::vector<StateFile::class_rule_workspace>;
    // Get length of file, REF: <https://cplusplus.com/reference/istream/istream/seekg/#I_content>
    this->fd.seekg(0, this->fd.end);
    std::size_t length = this->fd.tellg();
    this->fd.seekg(0, this->fd.beg);

    // Read file into memory, REF: <https://cplusplus.com/reference/istream/istream/seekg/#I_content>
    char *buffer = new char[length];
    this->fd.read(buffer, length);
    std::string state_file_content{buffer};
    delete[] buffer;

    return_type ret;
    // Deserialize file content
    while (state_file_content.find(this->magic) != std::string::npos) {
        std::string class_name, rule;
        int workspace;
        for (int _ = 0; _ < 3; ++_) {
            std::size_t magic_pos = state_file_content.find(this->magic);
            if (magic_pos == std::string::npos) { // malformed state filie
                // clear state file content
                this->fd.close();
                this->fd.open(this->path, std::ios_base::out | std::ios_base::trunc);
                this->fd.close();
                this->fd.open(this->path, std::ios_base::in | std::ios_base::app);
                return return_type{};
            }
            std::string value = state_file_content.substr(0, magic_pos);
            switch (_) {
                case 0:
                    class_name = value;
                    break;
                case 1:
                    rule = value;
                    break;
                case 2:
                    workspace = std::stoi(value.c_str());
                    break;
                default:
                    // unreachable code
                    assert("unreachable code" && false);
            };
            std::size_t next_start_pos = magic_pos + this->magic.length();
            state_file_content = state_file_content.substr(next_start_pos, state_file_content.length() - next_start_pos);
        }
        ret.push_back({class_name, rule, workspace});
    }
    return ret;
}

void StateFile::log(std::string const &class_name, std::string const &rule, int workspace) {
    std::string state_string = fmt::format("{1}{0}{2}{0}{3}{0}", this->magic, class_name, rule, workspace);
    this->fd << state_string;
}

bool StateFile::has(std::string const &class_name, std::string const &rule, int workspace) {
    for (auto const &[c, r, w]: this->deser()) {
        if (c == class_name && r == rule && w == workspace) {
            return true;
        }
    }
    return false;
}
