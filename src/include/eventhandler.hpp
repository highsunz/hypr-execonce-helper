#pragma once

#include <string>
#include <optional>
#include <vector>

class EventHandler {
public:
    std::string name;
    std::size_t nfield;

public:
    EventHandler(std::string const &name_, std::size_t nfield_);
    ~EventHandler() {}

public:
    std::optional<std::vector<std::string>> parse(std::string const &event) const;
};
