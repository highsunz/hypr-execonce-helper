#pragma once

#include <filesystem>
#include <vector>
#include <fstream>

// uint32 to little endian binary string
inline std::string const le_encode(uint32_t x) {
    char ret[4] = {0};
    for (int pos = 0; pos < 4; ++pos) {
        ret[pos] = (x >> (pos << 3)) & 0xff;
    }
    return std::string{ret};
}

class StateFile {
    using class_rule_workspace = std::tuple<std::string, std::string, int>;
    std::string const magic = le_encode(0x94915004);
    std::fstream fd;
    std::filesystem::path path;

private:
    std::vector<class_rule_workspace> deser();

public:
    StateFile(std::filesystem::path const &);
    ~StateFile();

public:
    void log(std::string const &, std::string const &, int);
    bool has(std::string const &, std::string const &, int);
};
