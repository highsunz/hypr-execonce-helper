#pragma once

#include <iostream>
#include <cstdlib>
#include <filesystem>

#include <fmt/core.h>

inline void warning(std::string const &msg) { std::cerr << "WARNING: " << msg; }

inline std::filesystem::path get_hyprland_instance_root() {
    char const *hyprland_instance_signature = std::getenv("HYPRLAND_INSTANCE_SIGNATURE");
    if (!hyprland_instance_signature) {
        throw std::runtime_error("environment variable 'HYPRLAND_INSTANCE_SIGNATURE' is not set");
    }
    return std::filesystem::path{fmt::format("/tmp/hypr/{}", hyprland_instance_signature)};
}
