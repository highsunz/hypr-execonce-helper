#include "eventhandler.hpp"
#include <iostream>

EventHandler::EventHandler(std::string const &name_, std::size_t nfield_): name(name_), nfield(nfield_) {}

std::optional<std::vector<std::string>> EventHandler::parse(std::string const &event) const {
    std::vector<std::string> fields;
    // Filter out other events
    for (std::size_t i = 1; i < event.length(); ++i) {
        if (event[i-1] == '>' && event[i] == '>') {
            if (event.substr(0, i - 1) != this->name) {
                return {};
            }
            break;
        }
    }
    // Split fields
    std::size_t start_pos = this->name.length() + 2;  // offset by 2 ccaused by ">>"
    while (start_pos < event.length()) {
        std::size_t end_pos = event.find(',', start_pos);
        if (end_pos == std::string::npos) {
            end_pos = event.length();
        }
        fields.push_back(event.substr(start_pos, end_pos - start_pos));
        start_pos = end_pos + 1;
    }
    // Return none if fields count does not add up
    if (fields.size() < this->nfield) {
        return {};
    }
    // Combine all redundant fields (if any) into last field and return
    for (std::size_t i = this->nfield; i < fields.size(); ++i) {
        fields[this->nfield-1] += "," + fields[i];
    }
    fields.erase(fields.begin() + this->nfield, fields.end());
    return fields;
}
