#include "execonce-helper.hpp"

void execute(std::string const &rule, int workspace) {
    int unset_cmd_status = std::system(fmt::format("hyprctl keyword windowrulev2 \"workspace unset, {}\"", rule).c_str());
    if (unset_cmd_status != 0) {
        warning(fmt::format("unsetting window rule returned non-zero status code {}\n", unset_cmd_status));
    }
    int set_cmd_status = std::system(fmt::format("hyprctl keyword windowrulev2 \"workspace {}, {}\"", workspace, rule).c_str());
    if (set_cmd_status != 0) {
        warning(fmt::format("setting new window rule returned non-zero status code {}\n", set_cmd_status));
    }
}

bool all_rules_applied(std::map<int, std::vector<Cfg>> const &assigns) {
    for (auto const &[_, cfgs]: assigns) {
        if (cfgs.size() > 0) {
            return false;
        }
    }
    return true;
}

int execonce_helper_process(
    std::vector<std::string> const &class_name_args,
    std::vector<std::string> const &rule_args,
    std::vector<int> const &workspace_args
) {
    if (class_name_args.size() != rule_args.size() || class_name_args.size() != workspace_args.size()) {
        throw std::runtime_error(fmt::format("Supplied different numbers of -c({}), -r({}), and -w({})\n", class_name_args.size(), rule_args.size(), workspace_args.size()));
    }
    StateFile state(get_hyprland_instance_root() / ".execonce-helper.state");

    std::map<int, std::vector<Cfg>> assigns;
    for (std::size_t i = 0; i < workspace_args.size(); ++i) {
        if (state.has(class_name_args[i], rule_args[i], workspace_args[i])) {
            execute(rule_args[i], workspace_args[i]);
        } else {
            assigns[workspace_args[i]].push_back(Cfg(rule_args[i], class_name_args[i]));
        }
    }

    EventHandler open_window_handler("openwindow", 4);
    for (std::string event; !all_rules_applied(assigns);) {
        std::getline(std::cin, event);
        auto parse_result = open_window_handler.parse(event);
        if (parse_result.has_value()) {
            std::vector<std::string> fields = parse_result.value();
            int workspace;
            std::string class_name;
            workspace = std::atoi(fields[1].c_str());
            if (workspace < 1 || workspace > max_workspaces) {
                warning(fmt::format("workspace number too large (got {}, expected in range [1, {}]\n", workspace, max_workspaces));
            }
            class_name = fields[2];
            for (std::size_t i = 0; i < assigns[workspace].size(); ++i) {
                Cfg const &cfg = assigns[workspace][i];
                execute(cfg.rule, workspace);
                state.log(cfg.class_name, cfg.rule, workspace);
                assigns[workspace].erase(assigns[workspace].begin() + i);
            }
        }
    }
    return 0;
}
