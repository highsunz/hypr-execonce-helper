#include "last-workspace-recorder.hpp"

int last_workspace_recorder_process() {
    std::filesystem::path out_path = get_hyprland_instance_root() / ".last-workspace";
    int last_workspace = 1;
    EventHandler workspace_handler("workspace", 1);

    // Write initial workspace
    std::ofstream ofd(out_path, std::ios_base::out | std::ios_base::trunc);
    std::string ws_ser = fmt::format("{}", last_workspace);
    ofd.write(ws_ser.c_str(), ws_ser.size());
    ofd.close();

    for (std::string event;;) {
        std::getline(std::cin, event);
        auto parse_result = workspace_handler.parse(event);
        if (parse_result.has_value()) {
            int workspace = std::stoi(parse_result.value()[0]);
            ofd.open(out_path, std::ios_base::out | std::ios_base::trunc);
            std::string ws_ser = fmt::format("{}", last_workspace);
            ofd.write(ws_ser.c_str(), ws_ser.size());
            ofd.close();
            last_workspace = workspace;
        }
    }
    return 0;
}
