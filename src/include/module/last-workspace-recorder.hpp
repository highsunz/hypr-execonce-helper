#pragma once

#include <iostream>
#include <fstream>
#include <filesystem>

#include <fmt/core.h>

#include <eventhandler.hpp>
#include <utils.hpp>

int last_workspace_recorder_process();
