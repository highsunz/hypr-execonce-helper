#pragma once

#include <map>

#include <fmt/core.h>

#include <eventhandler.hpp>
#include <statefile.hpp>
#include <utils.hpp>

int const max_workspaces = 512;
struct Cfg {
    std::string rule, class_name;
    Cfg(std::string const &rule_, std::string const &class_name_): rule(rule_), class_name(class_name_) {}
};

void execute(std::string const &rule, int workspace);

bool all_rules_applied(std::map<int, std::vector<Cfg>> const &assigns);

int execonce_helper_process(
    std::vector<std::string> const &class_name_args,
    std::vector<std::string> const &rule_args,
    std::vector<int> const &workspace_args
);
