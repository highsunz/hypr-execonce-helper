#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>

#include <fmt/core.h>
#include <cli2b.hpp>

#include <eventhandler.hpp>
#include <statefile.hpp>
#include <utils.hpp>

#include <module/execonce-helper.hpp>
#include <module/last-workspace-recorder.hpp>

cli2b::CLI get_cli(std::string const &prog_name) {
    cli2b::CLI execonce_helper("execonce-helper",
        "Set up windowrulev2 after `exec-once`", {
        cli2b::EntryDefinition()
            .short_name("c")
            .long_name("class-name")
            .option_type(cli2b::EntryType::STRING)
            .help_msg("specify class name to match"),
        cli2b::EntryDefinition()
            .short_name("r")
            .long_name("rule")
            .option_type(cli2b::EntryType::STRING)
            .help_msg("specify rule to apply"),
        cli2b::EntryDefinition()
            .short_name("w")
            .long_name("workspace")
            .option_type(cli2b::EntryType::INT)
            .help_msg("specify workspace to listen actions on"),
        cli2b::EntryDefinition()
            .short_name("h")
            .long_name("help")
            .option_type(cli2b::EntryType::FLAG)
            .help_msg("show help message"),
    }, {});
    cli2b::CLI last_workspace_recorder("last-workspace-recorder",
        "Record last-used workspace", {
        cli2b::EntryDefinition()
            .short_name("h")
            .long_name("help")
            .option_type(cli2b::EntryType::FLAG)
            .help_msg("show help message"),
    }, {});
    return cli2b::CLI(prog_name,
        "Handle messages comming from hyprland's control socket", {
        cli2b::EntryDefinition()
            .short_name("h")
            .long_name("help")
            .option_type(cli2b::EntryType::FLAG)
            .help_msg("show help message"),
    }, {execonce_helper, last_workspace_recorder});
}

int main(int argc, char const **argv) {
    std::string prog_name = "hypr-msg-handler";
    cli2b::CLI cli = get_cli(prog_name);
    cli2b::CLI const &execonce_helper = cli.get_subcommand("execonce-helper");
    cli2b::CLI const &last_workspace_recorder = cli.get_subcommand("last-workspace-recorder");

    std::size_t read_options = cli.parse(argc, argv);
    if (cli.get_parsed_argv_cnt() == 0 || cli.occurrence_long("help") > 0) {
        std::cout << cli.get_help_msg();
        return 0;
    }
    if (cli.occurrence_subcommand("execonce-helper")) {
        if (read_options == 0 || execonce_helper.occurrence_long("help") > 0) {
            std::cout << execonce_helper.get_help_msg();
            return 0;
        }
        auto class_name_args = execonce_helper.get_long<std::string>("class-name");
        auto rule_args = execonce_helper.get_long<std::string>("rule");
        auto workspace_args = execonce_helper.get_long<int>("workspace");
        return execonce_helper_process(class_name_args, rule_args, workspace_args);
    } else if (cli.occurrence_subcommand("last-workspace-recorder")) {
        if (last_workspace_recorder.occurrence_long("help") > 0) {
            std::cout << last_workspace_recorder.get_help_msg();
            return 0;
        }
        return last_workspace_recorder_process();
    } else {
        std::cout << cli.get_help_msg();
    }
    return 0;
}
