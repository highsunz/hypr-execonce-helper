{
  description = "Helper for setting up windowrulev2 on hyprland after exec-once";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/release-23.11";
    flake-utils.url = "github:numtide/flake-utils";

    cli2b = {
      url = "gitlab:highsunz/cli2b";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = inputs@{ self, nixpkgs, flake-utils, ... }: flake-utils.lib.eachSystem [ "aarch64-linux" "x86_64-linux" ] (system: let
    pkgs = import nixpkgs {
      inherit system;
    };
    lib = nixpkgs.lib;
  in rec {
    hydraJobs = {
      inherit (packages) hypr-msg-handler;
    };
    packages = with pkgs; let
      assertHyprInstanceSignatureScript = ''
        set -Eeuo pipefail

        if [[ -z $HYPRLAND_INSTANCE_SIGNATURE ]]; then
          echo "E: HYPRLAND_INSTANCE_SIGNATURE is not set" >&2
          exit 1
        fi
        # Avoid reading from stdin so that socat runs properly in a systemd service
        # REF: <https://unix.stackexchange.com/a/218597/519811>
        ${socat}/bin/socat -u UNIX-CONNECT:/tmp/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock STDOUT |
      '';
    in rec {
      default = hypr-msg-handler;
      hypr-msg-handler = stdenv.mkDerivation {
        pname = "hypr-msg-handler";
        version = "0.1.0";
        src = ./src;
        nativeBuildInputs = [ meson ninja pkg-config ];
        mesonBuildType = "release";
        buildInputs = [
          inputs.cli2b.packages.${system}.cli2b
          fmt
        ];
        shellHook = ''
          [[ "$-" == *i* ]] && exec "$SHELL"
        '';

        meta = {
          homepage = "https://gitlab.com/highsunz/hypr-msg-handler";
          license = lib.licenses.mit;
        };
      };
      hypr-execonce-helper = writeShellScript "hypr-execonce-helper" (assertHyprInstanceSignatureScript + ''
        ${hypr-msg-handler}/bin/hypr-msg-handler execonce-helper "$@"
      '');
      hypr-last-workspace-recorder = writeShellScript "hypr-last-workspace-recorder" (assertHyprInstanceSignatureScript + ''
        while true; do
          ${hypr-msg-handler}/bin/hypr-msg-handler last-workspace-recorder "$@"
        done
      '');
    };
  });
}
